package com.doit.hbase_demo;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class HbaseDemo2 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Admin admin = conn.getAdmin();

        //表的描述器需要表的描述器构建器  构建出来
        TableDescriptorBuilder descriptorBuilder = TableDescriptorBuilder.newBuilder(TableName.valueOf("doit_info"));

        ColumnFamilyDescriptorBuilder cfdb = ColumnFamilyDescriptorBuilder.newBuilder("base_info".getBytes());

        ColumnFamilyDescriptor cfbuilder = cfdb.build();

        descriptorBuilder.setColumnFamily(cfbuilder);

        //通过表的描述器构建表
        TableDescriptor build = descriptorBuilder.build();

        admin.createTable(build);

    }
}
