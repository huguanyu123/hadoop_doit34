package com.doit.hbase_demo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

public class HbaseDemo1 {
    public static void main(String[] args) throws IOException {
        // 获取Hbase的配置信息
        Configuration conf = HBaseConfiguration.create();

        conf.set("hbase.zookeeper.quorum", "doit01:2181,doit02:2181,doit03:2181");

        Connection conn = ConnectionFactory.createConnection(conf);

        // Table table = conn.getTable(TableName.valueOf("user_info"));
        // 获取hbase的操作对象
        Admin admin = conn.getAdmin();

        TableName[] ts = admin.listTableNames();

        for (TableName t : ts) {
            System.out.println(t.getNameAsString());
        }

        conn.close();

    }
}
