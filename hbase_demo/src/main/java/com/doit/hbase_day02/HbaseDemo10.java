package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;
import java.util.Iterator;

// 前三条数据
// 001 - 003
// select * from emp limit 3;
public class HbaseDemo10 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        Scan scan = new Scan();

        // 设置起始行
        scan.withStartRow("004".getBytes());

        // 设置结束行
        scan.withStopRow("009".getBytes());

        // 设置显示条数
        scan.setLimit(3);

        ResultScanner rs = tb.getScanner(scan);

        Iterator<Result> it = rs.iterator();

        while (it.hasNext()) {
            Result r = it.next();

            while (r.advance()) {
                HbaseUtil.cellParse(r);
            }
        }

        rs.close();
        conn.close();


    }
}
