package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class HbaseDemo11 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        Get get = new Get(Bytes.toBytes("001"));

//        get.addFamily(Bytes.toBytes("base_info"));

        get.addColumn(Bytes.toBytes("base_info"),Bytes.toBytes("name"));
        get.addColumn(Bytes.toBytes("base_info"),Bytes.toBytes("gender"));
        get.addColumn(Bytes.toBytes("base_info"),Bytes.toBytes("age"));

        Result r = tb.get(get);

        while (r.advance()) {
            HbaseUtil.cellParse(r);
        }

        conn.close();
    }
}
