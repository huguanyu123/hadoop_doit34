package com.doit.day01;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HDFSFileDemo2 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        Configuration conf = new Configuration();

        URI uri = new URI("hdfs://doit01:8020/");

        FileSystem fs = FileSystem.newInstance(uri, conf, "root");

        Path src = new Path("/wordCount.txt");

        if (fs.exists(src)) {
//            fs.rename(src, new Path("/wordCount.txt"));
            fs.rename(src, new Path("/huguanyu/wdc.txt"));
        }

        fs.close();
    }
}
