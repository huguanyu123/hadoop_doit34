package com.doit.day01;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HDFSFileDemo3 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        Configuration conf = new Configuration();

        URI uri = new URI("hdfs://doit01:8020/");

        FileSystem fs = FileSystem.newInstance(uri, conf, "root");

        Path src = new Path("/huguanyu");

        if (fs.exists(src)) {
            fs.delete(src, true);
        }

        fs.close();
    }
}
