package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class HbaseDemo1 {
    public static void main(String[] args) throws IOException {
        // 创建了一张空表

        Connection conn = HbaseUtil.getConn();
        // 获取操作hbase的用户
        Admin admin = conn.getAdmin();

        // 创建一张新表(通过表的描述器创建)
        // 表的描述器需要构建器构建
        TableDescriptorBuilder tdb = TableDescriptorBuilder.newBuilder(TableName.valueOf("doit_stu"));

        // 创建表描述器
        TableDescriptor build = tdb.build();

        // 创建一张表
        admin.createTable(build);

        conn.close();

    }
}
