package com.doit.day01;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HDFSDownFileDemo1 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        FileSystem fs = FileSystem.newInstance(new URI("hdfs://doit01:8020/"), new Configuration(), "root");

        fs.copyToLocalFile(new Path("/wdcount.txt"), new Path("D:\\"));

        fs.close();
    }
}
