package com.doit;

public class JavaTest {
    public String splitStr(String str, String regex, String appStr) {
        String[] sp = str.split(regex);

        StringBuilder line = new StringBuilder();
        for (String s : sp) {
            line.append(s).append(appStr);
        }

        return line.toString();
    }
}
