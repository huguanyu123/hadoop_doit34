package com.doit.day04;

import com.alibaba.fastjson.JSONObject;
import com.doit.day02.Movie;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
 4.每部电影评论最高分的n条记录
 */
public class MovieMRDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setJarByClass(MovieMRDemo.class);

        job.setMapperClass(MovieMapper4.class);
        job.setReducerClass(MovieReducer4.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Movie.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.waitForCompletion(true);
    }

    static class MovieMapper4 extends Mapper<LongWritable, Text, Text, Movie> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String movieJson = value.toString();

            //将movieJson转成movie对象
            Movie mv = JSONObject.parseObject(movieJson, Movie.class);

            Text k = new Text(mv.getMovie());

            context.write(k, mv);
        }
    }


    static class MovieReducer4 extends Reducer<Text, Movie, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Movie> values, Context context) throws IOException, InterruptedException {
            ArrayList<Movie> movieList = new ArrayList<Movie>();

            for (Movie value : values) {

                Movie movie = new Movie(value.getMovie(), value.getRate(), value.getTimeStamp(), value.getUid());

                movieList.add(movie);
            }


            //对集合进行排序
            Collections.sort(movieList, new Comparator<Movie>() {
                public int compare(Movie o1, Movie o2) {
                    return Integer.parseInt(o2.getRate()) - Integer.parseInt(o1.getRate());
                }
            });

            //将top3的记录用StringBuilder进行拼接  并且输出
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < movieList.size(); i++) {
                if (i < 3) {
                    sb.append(movieList.get(i).toString()).append("---");
                }
            }

            context.write(key, new Text(sb.toString()));

        }
    }
}
