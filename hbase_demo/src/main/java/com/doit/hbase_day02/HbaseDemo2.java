package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;

public class HbaseDemo2 {
    public static void main(String[] args) throws IOException {
        // 创建了一张带一个列族的表
        Connection conn = HbaseUtil.getConn();
        // 获取操作hbase的用户
        Admin admin = conn.getAdmin();

        // 创建一张新表(通过表的描述器创建)
        // 表的描述器需要构建器构建
        TableDescriptorBuilder tdb = TableDescriptorBuilder.newBuilder(TableName.valueOf("doit34:doit_stu"));

        // 列族的描述器对象  列族的描述器的构建器创建
        ColumnFamilyDescriptorBuilder cdb = ColumnFamilyDescriptorBuilder.newBuilder("f1".getBytes());

        // 创建一个列族描述器对象
        ColumnFamilyDescriptor cd = cdb.build();

        // 添加列族
        tdb.setColumnFamily(cd);

        // 创建表描述器
        TableDescriptor build = tdb.build();

        // 创建一张表
        admin.createTable(build);

        conn.close();

    }
}
