package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class HbaseDemo12 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        Delete delete = new Delete(Bytes.toBytes("001"));

        //删除一个单元格
//        delete.add()

        // 删除一个列族
//        delete.addFamily(Bytes.toBytes("ext_info"));
        delete.addFamily(Bytes.toBytes("base_info"));

        // 删除某个列族中的某个属性
//        delete.addColumn()

        tb.delete(delete);

        conn.close();

    }
}
