package com.doit.day03;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.CombineTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.Random;

public class WordCount2Demo {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setMapperClass(WordCountMapper.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //设置ReduceTask的数量
        job.setNumReduceTasks(3);

        job.setPartitionerClass(MyPartitioner.class);

        //预聚合操作
        job.setCombinerClass(MyCombiner.class);

        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\wdcount.txt"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\wdcount_value"));

        job.waitForCompletion(true);
    }

    static class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String s = value.toString();

            String[] sp = s.split("\\s+");

            for (String s1 : sp) {
                context.write(new Text(s1), new IntWritable(1));
            }
        }
    }
}

class MyCombiner extends Reducer<Text, IntWritable, Text, IntWritable> {
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int count = 0;
        for (IntWritable value : values) {
            count++;
        }

        context.write(key, new IntWritable(count));
    }
}


class MyPartitioner extends Partitioner<Text, IntWritable> {
//    static long count = 0;

    //public int getPartition(K k, V v, int numPartitions) {

//        count++;

//        return (int) count % numPartitions;

    //}

    public int getPartition(Text k, IntWritable v, int numPartitions) {
        Random rd = new Random();

        int num = rd.nextInt();

        return ((k.toString() + num).hashCode() & Integer.MAX_VALUE) % numPartitions;
    }
}