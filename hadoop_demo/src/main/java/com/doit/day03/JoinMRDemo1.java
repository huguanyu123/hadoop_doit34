package com.doit.day03;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.ArrayList;

public class JoinMRDemo1 {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setMapperClass(JoinMapper.class);
        job.setReducerClass(JoinReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);


        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);

        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\join"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\join_value"));

        job.waitForCompletion(true);
    }

    static class JoinMapper extends Mapper<LongWritable, Text, Text, Text> {
        String name;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            FileSplit fs = (FileSplit) context.getInputSplit();
            name = fs.getPath().getName();
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String s = value.toString();

            if (name.equals("order.txt")) {
                String[] sp = s.split(",");

                context.write(new Text(sp[1]), new Text(sp[0]));
            } else {
                String[] sp = s.split("-", 2);

                context.write(new Text(sp[0]), new Text(sp[1]));
            }
        }
    }

    static class JoinReducer extends Reducer<Text, Text, Text, NullWritable> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            // 创建一个集合  用于保存订单
            ArrayList<String> orderList = new ArrayList<String>();

            ArrayList<String> userList = new ArrayList<String>();

            for (Text value : values) {
                String s = value.toString();

                if (s.contains("order")) {
                    orderList.add(s);
                } else {
                    userList.add(s);
                }
            }

            // key   user01   list [order01,order02]  str 张三-M-20
            for (String ss : orderList) {
                String str = ss + "-" + key + "-" + userList.get(0);

                context.write(new Text(str), NullWritable.get());
            }

        }
    }
}
