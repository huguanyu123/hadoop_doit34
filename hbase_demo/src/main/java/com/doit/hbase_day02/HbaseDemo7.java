package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

public class HbaseDemo7 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        // 添加rowkey
        Put p1 = new Put("007".getBytes());
        // 往改行添加数据
//        p1.addColumn("base_info".getBytes(), "name".getBytes(), "刘德华".getBytes());
        p1.addColumn("base_info".getBytes(), "age".getBytes(), "50".getBytes());
        p1.addColumn("base_info".getBytes(), "gender".getBytes(), "M".getBytes());
        p1.addColumn("ext_info".getBytes(), "color".getBytes(), "yellow".getBytes());
        p1.addColumn("ext_info".getBytes(), "music".getBytes(), "笨小孩儿".getBytes());
        p1.addColumn("ext_info".getBytes(), "address".getBytes(), "hongkong".getBytes());


        tb.put(p1);

        conn.close();

    }
}
