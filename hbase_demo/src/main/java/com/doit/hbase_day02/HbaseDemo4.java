package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;

public class HbaseDemo4 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Admin admin = conn.getAdmin();

        TableDescriptorBuilder tdb = TableDescriptorBuilder.newBuilder(TableName.valueOf("doit34:region_tb"));

        ColumnFamilyDescriptorBuilder cfd1 = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes("f1"));
        ColumnFamilyDescriptorBuilder cfd2 = ColumnFamilyDescriptorBuilder.newBuilder(Bytes.toBytes("f2"));

        ColumnFamilyDescriptor cf1 = cfd1.build();
        ColumnFamilyDescriptor cf2 = cfd2.build();

        ArrayList<ColumnFamilyDescriptor> list = new ArrayList<>();

        list.add(cf1);
        list.add(cf2);

        tdb.setColumnFamilies(list);

        TableDescriptor tb = tdb.build();

        byte[][] bys = {Bytes.toBytes("100"), Bytes.toBytes("200"), Bytes.toBytes("300")};

        // 创建了一个region表
        admin.createTable(tb, bys);

        admin.close();
        conn.close();

    }
}
