package com.doit.day02;

import java.io.*;

public class SerializableDemo1 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        serObject(); //序列化
        reverseObject(); //反序列化
    }

    private static void reverseObject() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:\\person.txt"));

//        Object obj = ois.readObject();
        String name = ois.readUTF();
        int age = ois.readInt();


        System.out.println(name + age);
    }

    private static void serObject() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:\\person.txt"));

        Person per = new Person("傅思琪", 27);

//        oos.writeObject(per);

        oos.writeUTF(per.getName());
        oos.writeInt(per.getAge());

        oos.close();
    }

}


class Person implements Serializable {
    private String name;
    private int age;
    private static final long serialVersionUID = 42L;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}