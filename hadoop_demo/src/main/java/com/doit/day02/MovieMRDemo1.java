package com.doit.day02;

import com.alibaba.fastjson.JSONObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class MovieMRDemo1 {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setMapperClass(MovieMapper.class);
        job.setReducerClass(MovieReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(DoubleWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(DoubleWritable.class);

        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\movie"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\movie_value"));

        job.waitForCompletion(true);
    }

    static class MovieMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {
        String name = null;
        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            FileSplit fs = (FileSplit) context.getInputSplit();
            name = fs.getPath().getName();
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

            String movieJson = value.toString();

            //将json数据进行解析  解析为movie对象
            Movie movie = JSONObject.parseObject(movieJson, Movie.class);

            Text k = new Text(movie.getMovie());
            DoubleWritable v = new DoubleWritable(Double.parseDouble(movie.getRate()));

            context.write(k, v);
        }
    }


    static class MovieReducer extends Reducer<Text, DoubleWritable, Text, DoubleWritable> {
        @Override
        protected void reduce(Text key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
            double sum = 0;
            int count = 0;

            for (DoubleWritable value : values) {
                double d = value.get();
                sum += d;
                count++;
            }

            double avg = sum / count;

            context.write(key, new DoubleWritable(avg));
        }
    }
}
