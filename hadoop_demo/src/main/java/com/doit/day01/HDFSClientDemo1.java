package com.doit.day01;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HDFSClientDemo1 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        //创建配置文件对象
        Configuration conf = new Configuration();

        conf.set("dfs.blocksize", "300M");
        conf.set("dfs.replication", "2");

        //创建文件系统所需的uri对象
        URI uri = new URI("hdfs://doit01:8020/");


        //1.先创建hdfs文件系统对象
        FileSystem fs = FileSystem.newInstance(uri, conf, "root");

        //创建源文件路径对象
        Path src = new Path("D:\\hdp_data\\wdcount.txt");

        //创建目的地路径对象
        Path dest = new Path("/");

        //2.上传文件
        fs.copyFromLocalFile(src, dest);

        fs.close();
    }
}
