package com.doit.hbase_util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Result;

import java.io.IOException;

public class HbaseUtil {
    public static Connection getConn() throws IOException {
        // 获取Hbase的配置信息
        Configuration conf = HBaseConfiguration.create();

        conf.set("hbase.zookeeper.quorum", "doit01:2181,doit02:2181,doit03:2181");

        return ConnectionFactory.createConnection(conf);
    }


    public static void cellParse(Result r) {
        Cell cell = r.current();

        byte[] row = CellUtil.cloneRow(cell);
        byte[] family = CellUtil.cloneFamily(cell);
        byte[] qualifier = CellUtil.cloneQualifier(cell);
        byte[] value = CellUtil.cloneValue(cell);

        System.out.println(new String(row) + "--" + new String(family) + "--" + new String(qualifier) + "--" + new String(value));

    }
}
