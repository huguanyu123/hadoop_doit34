package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;

public class HbaseDemo5 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Admin admin = conn.getAdmin();

        TableName tn = TableName.valueOf("doit34:delete_tb");

        if (admin.tableExists(tn)) {
            if (!admin.isTableDisabled(tn)) {
                admin.disableTable(tn);
            }
            admin.deleteTable(tn);
        } else {
            System.out.println("表都没有,你删什么????");
        }

        admin.close();
        conn.close();

    }
}
