package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;

import java.io.IOException;
import java.util.ArrayList;

public class HbaseDemo8 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        // 添加rowkey
        Put p1 = new Put("007".getBytes());
        // 往改行添加数据
        p1.addColumn("base_info".getBytes(), "name".getBytes(), "jay".getBytes());
        p1.addColumn("base_info".getBytes(), "age".getBytes(), "40".getBytes());
        p1.addColumn("base_info".getBytes(), "gender".getBytes(), "M".getBytes());
        p1.addColumn("ext_info".getBytes(), "color".getBytes(), "yellow".getBytes());
        p1.addColumn("ext_info".getBytes(), "music".getBytes(), "夜曲".getBytes());
        p1.addColumn("ext_info".getBytes(), "address".getBytes(), "wanwan".getBytes());

        // 添加rowkey
        Put p2 = new Put("008".getBytes());
        // 往改行添加数据
        p2.addColumn("base_info".getBytes(), "name".getBytes(), "jolin".getBytes());
        p2.addColumn("base_info".getBytes(), "age".getBytes(), "43".getBytes());
        p2.addColumn("base_info".getBytes(), "gender".getBytes(), "F".getBytes());
        p2.addColumn("ext_info".getBytes(), "color".getBytes(), "yellow".getBytes());
        p2.addColumn("ext_info".getBytes(), "music".getBytes(), "看我72变".getBytes());
        p2.addColumn("ext_info".getBytes(), "address".getBytes(), "wanwan".getBytes());

        // 添加rowkey
        Put p3 = new Put("009".getBytes());
        // 往改行添加数据
        p3.addColumn("base_info".getBytes(), "name".getBytes(), "eson".getBytes());
        p3.addColumn("base_info".getBytes(), "age".getBytes(), "55".getBytes());
        p3.addColumn("base_info".getBytes(), "gender".getBytes(), "F".getBytes());
        p3.addColumn("ext_info".getBytes(), "color".getBytes(), "white".getBytes());
        p3.addColumn("ext_info".getBytes(), "music".getBytes(), "爱情转移".getBytes());
        p3.addColumn("ext_info".getBytes(), "address".getBytes(), "hongkong".getBytes());

        // 添加rowkey
        Put p4 = new Put("010".getBytes());
        // 往改行添加数据
        p4.addColumn("base_info".getBytes(), "name".getBytes(), "amei".getBytes());
        p4.addColumn("base_info".getBytes(), "age".getBytes(), "36".getBytes());
        p4.addColumn("base_info".getBytes(), "gender".getBytes(), "F".getBytes());
        p4.addColumn("ext_info".getBytes(), "color".getBytes(), "black".getBytes());
        p4.addColumn("ext_info".getBytes(), "music".getBytes(), "听海".getBytes());
        p4.addColumn("ext_info".getBytes(), "address".getBytes(), "hongkong".getBytes());


        ArrayList<Put> list = new ArrayList<>();

        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);

        tb.put(list);

        conn.close();

    }
}
