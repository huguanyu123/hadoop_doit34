package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

import java.io.IOException;
import java.util.Iterator;

public class HbaseDemo9 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        //获取表对象
        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        Scan scan = new Scan();

        //开始查询
        ResultScanner scanner = tb.getScanner(scan);

        // 转化为结果集迭代器
        Iterator<Result> it = scanner.iterator();

        // 遍历it对象
        while (it.hasNext()) {

            // 一整行数据
            Result rs = it.next();

            while (rs.advance()) {
                // 拿到单元格对象
                Cell cell = rs.current();

                byte[] rowArray = CellUtil.cloneRow(cell);
                byte[] familyArray = CellUtil.cloneFamily(cell);
                byte[] qualifierArray = CellUtil.cloneQualifier(cell);
                byte[] valueArray = CellUtil.cloneValue(cell);

                System.out.println(new String(rowArray) + "--"
                        + new String(familyArray)
                        + "--" + new String(qualifierArray)
                        + "--" + new String(valueArray));

            }
        }

        scanner.close();
        conn.close();


        /*
                byte[] rowArray = cell.getRowArray();
                byte[] familyArray = cell.getFamilyArray();
                byte[] qualifierArray = cell.getQualifierArray();
                byte[] valueArray = cell.getValueArray();

         */
    }
}
