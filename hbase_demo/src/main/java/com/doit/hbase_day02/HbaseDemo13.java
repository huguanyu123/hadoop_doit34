package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.exceptions.DeserializationException;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Iterator;

public class HbaseDemo13 {
    public static void main(String[] args) throws IOException, DeserializationException {
        Connection conn = HbaseUtil.getConn();

        Table tb = conn.getTable(TableName.valueOf("doit34:user_info"));

        Scan scan = new Scan();

        ValueFilter vf = new ValueFilter(CompareOperator.EQUAL, new BinaryComparator(Bytes.toBytes("white")));
        RowFilter rf = new RowFilter(CompareOperator.GREATER, new BinaryComparator(Bytes.toBytes("005")));

        // 组合过滤器
        scan.setFilter(vf);
        scan.setFilter(rf);

        // 组合过滤器的添加方式
        FilterList fl = new FilterList();
        fl.addFilter(vf);
        fl.addFilter(rf);

        scan.setFilter(fl);

        ResultScanner rs = tb.getScanner(scan);

        Iterator<Result> it = rs.iterator();

        while (it.hasNext()) {
            Result r = it.next();

            while (r.advance()) {
                HbaseUtil.cellParse(r);
            }
        }

        rs.close();
        conn.close();

    }
}
