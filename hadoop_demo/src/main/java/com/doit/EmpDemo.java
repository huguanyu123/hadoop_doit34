package com.doit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class EmpDemo {
    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("D://emp.txt"));

        String[] letters = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

        Random rd = new Random();


        for (int i = 1; i <= 100000; i++) {
            ArrayList<Integer> in = new ArrayList<Integer>();
            for (int j = 0; j < 5; j++) {
                int index = rd.nextInt(letters.length);
                in.add(index);
            }

            String name = "";

            for (Integer is : in) {
                name += letters[is];
            }

            int money = rd.nextInt(30000);
            bw.write(i + "," + name + "," + money);
            bw.newLine();

            if (i == 500) {
                bw.flush();
            }
        }

        bw.close();


    }
}

