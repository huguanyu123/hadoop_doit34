package com.doit.day01;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URISyntaxException;

/*
元数据:
    存在于NN上面

    元数据存在于内存中还是磁盘上??
    内存中:
        如果NN宕机了  那么所有的元数据丢失了

    磁盘上:
        保证元数据安全  但是IO增多  导致速度变慢

 */

public class HDFSFileDemo7 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        FileSystem fs = FsUtil.getFileSystem();

        FSDataOutputStream fo = fs.create(new Path("/Hello.java"));

        fo.write("IO流不能忘啊!!!".getBytes());

        fo.close();
        fs.close();
    }
}
