package com.doit.day04;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

public class JoinMRDemo1 {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setJarByClass(JoinMRDemo1.class);

        //设置分布式集群缓存文件   不能在本地运行
        job.setCacheFiles(new URI[]{new URI("hdfs://doit01:8020/join/user.txt")});

        job.setMapperClass(JoinMapper.class);

        job.setMapOutputKeyClass(Text.class);

        job.setMapOutputValueClass(NullWritable.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        job.waitForCompletion(true);
    }

    static class JoinMapper extends Mapper<LongWritable, Text, Text, NullWritable> {
        HashMap<String, String> hm = null;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            BufferedReader br = new BufferedReader(new FileReader("user.txt"));

            hm = new HashMap<String, String>();

            String line = null;

            while ((line = br.readLine()) != null) {
                String[] sp = line.split("-", 2);
                hm.put(sp[0], sp[1]);
            }
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String[] sp = value.toString().split(",");

            if (hm.containsKey(sp[1])) {
                context.write(new Text(sp[1] + sp[0] + hm.get(sp[1])), NullWritable.get());
            }
        }
    }
}
