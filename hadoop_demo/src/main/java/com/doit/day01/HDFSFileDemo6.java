package com.doit.day01;

import org.apache.hadoop.fs.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Random;

public class HDFSFileDemo6 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        FileSystem fs = FsUtil.getFileSystem();

        Path path = new Path("/wdcount.txt");

        FSDataInputStream input = fs.open(path);

        BufferedReader br = new BufferedReader(new InputStreamReader(input));

        br.skip(new Random().nextInt(100));

        System.out.println(br.readLine());
    }
}
