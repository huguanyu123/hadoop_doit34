package com.doit.hbase_day03;

import com.alibaba.fastjson.JSONObject;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MD5Hash;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;

import java.io.IOException;

/*
MovieWritable  不能为key   不然传不过去

 */

public class MovieMRDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = HBaseConfiguration.create();
        // 设置hbase的地址
        conf.set("hbase.zookeeper.quorum", "doit01:2181,doit02:2181,doit03:2181");
        Job job = Job.getInstance(conf);
        // 设置mapper
        job.setMapperClass(MovieMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(MovieWritable.class);
        // 输入
        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\movie"));

        // 设置reducer相关  --->hbase中写数据
        TableMapReduceUtil.initTableReducerJob("doit34:movie_tb", MovieReducer.class, job);
        // 提交程序
        job.waitForCompletion(true);

    }

    static class MovieMapper extends Mapper<LongWritable, Text, Text, MovieWritable> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();

            MovieWritable mv = JSONObject.parseObject(line, MovieWritable.class);

            System.out.println("mapper" + mv);

            StringBuilder time = new StringBuilder(System.currentTimeMillis() + "");

            String s = time.reverse().substring(0, 7);

            String rk = MD5Hash.digest(s).toString().substring(0, 6);

            context.write(new Text(rk), mv);
        }
    }

    static class MovieReducer extends TableReducer<Text, MovieWritable, ImmutableBytesWritable> {
        @Override
        protected void reduce(Text key, Iterable<MovieWritable> values, Context context) throws IOException, InterruptedException {

            MovieWritable mv = values.iterator().next();

            Put put = new Put(Bytes.toBytes(key.toString()));

            System.out.println("reducer" + key);

            put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("movie"), Bytes.toBytes(mv.getMovie()));
            put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("rate"), Bytes.toBytes(mv.getRate()));
            put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("timeStamp"), Bytes.toBytes(mv.getTimeStamp()));
            put.addColumn(Bytes.toBytes("info"), Bytes.toBytes("uid"), Bytes.toBytes(mv.getUid()));

            context.write(null, put);
        }
    }
}


