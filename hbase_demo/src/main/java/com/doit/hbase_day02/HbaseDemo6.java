package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import javafx.scene.control.Tab;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

public class HbaseDemo6 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        Admin admin = conn.getAdmin();

        TableName tn = TableName.valueOf("doit34:doit_stu2");

//        ColumnFamilyDescriptorBuilder cfb = ColumnFamilyDescriptorBuilder.newBuilder("f1".getBytes());
//        cfb.setMaxVersions(5);

        //更改列族信息
//        admin.modifyColumnFamily(tn, cfb.build());

        // 删除列族2
        admin.deleteColumnFamily(tn, "f2".getBytes());

        admin.close();
        conn.close();

    }
}
