package com.doit.day01;

import org.apache.hadoop.fs.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

public class HDFSFileDemo5 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        FileSystem fs = FsUtil.getFileSystem();

        Path path = new Path("/");

        RemoteIterator<LocatedFileStatus> list = fs.listFiles(path, true);

        while (list.hasNext()) {
            //获取文件对象
            LocatedFileStatus status = list.next();

            System.out.println(status.getPath());
            System.out.println(status.getPermission());
            System.out.println(status.getBlockSize());
            System.out.println(status.getReplication());

            //获取文件在hdfs中存储的对应的块信息
            BlockLocation[] bts = status.getBlockLocations();

            for (BlockLocation bt : bts) {
                System.out.println(bt.getLength());
                System.out.println(Arrays.toString(bt.getNames()));
                System.out.println(Arrays.toString(bt.getHosts()));
                System.out.println(Arrays.toString(bt.getStorageTypes()));
                System.out.println(Arrays.toString(bt.getStorageIds()));
                System.out.println(bt.getOffset());
            }

            System.out.println("=============================");
        }

    }
}
