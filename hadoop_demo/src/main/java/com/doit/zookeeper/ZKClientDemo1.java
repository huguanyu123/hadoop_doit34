package com.doit.zookeeper;

import org.apache.zookeeper.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class ZKClientDemo1 {
    ZooKeeper zkClient;

    @Before
    public void createClient() throws IOException {
        String connectString = "doit01:2181,doit02:2181,doit03:2181";

        int timeout = 5000;

        // 创建连接的客户端
        zkClient = new ZooKeeper(connectString, timeout, new Watcher() {
            public void process(WatchedEvent watchedEvent) {
                try {
                    getNode();
                } catch (KeeperException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Test
    public void addNode() throws KeeperException, InterruptedException {
        zkClient.create("/animal", "小动物".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }

    @Test
    public void changeNode() throws KeeperException, InterruptedException {
        // -1表示无所谓   只要不是-1  就证明必须要在当前版本号下进行修改   锁机制
//        zkClient.setData("/student", "多易的所有学生".getBytes(), 0);
        zkClient.setData("/student", "多易的学生".getBytes(), -1);
    }

    @Test
    public void getNode() throws KeeperException, InterruptedException {
        List<String> list = zkClient.getChildren("/", null);

        for (String s : list) {
            System.out.println(s);
        }
    }

}
