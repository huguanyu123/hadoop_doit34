package com.doit.day01;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class FsUtil {
    public static FileSystem getFileSystem() throws URISyntaxException, IOException, InterruptedException {
        Configuration conf = new Configuration();

        URI uri = new URI("hdfs://doit01:8020/");

        return FileSystem.newInstance(uri, conf, "root");
    }
}
