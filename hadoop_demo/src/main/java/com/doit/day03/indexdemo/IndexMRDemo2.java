package com.doit.day03.indexdemo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;


public class IndexMRDemo2 {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setMapperClass(IndexMapper2.class);
        job.setReducerClass(IndexReducer2.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);

        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\index_value"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\index_value2"));

        job.waitForCompletion(true);
    }

    // kvkvkvkvkvkvkvkvkvkvkkvkvkvkvkv
    /*
    /*
hello-a.txt 2
hello-b.txt 2
hello-c.txt 3
hello-d.txt 2
 */
    static class IndexMapper2 extends Mapper<Text, IntWritable, Text, Text> {

        @Override
        protected void map(Text key, IntWritable value, Context context) throws IOException, InterruptedException {
            String s = key.toString();

            String[] sp = s.split("-");

            //hello  a.txt--2
            //hello  b.txt--2
            context.write(new Text(sp[0]), new Text(sp[1] + "--" + value.get()));

        }
    }

    static class IndexReducer2 extends Reducer<Text, Text, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            StringBuilder sb = new StringBuilder();

            for (Text value : values) {
                sb.append(value.toString()).append("\t");
            }

            context.write(key, new Text(sb.toString()));
        }
    }
}
