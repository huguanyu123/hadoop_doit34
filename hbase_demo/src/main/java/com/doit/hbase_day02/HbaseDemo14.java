package com.doit.hbase_day02;

import com.doit.hbase_util.HbaseUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.regionserver.StoreFileManager;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class HbaseDemo14 {
    public static void main(String[] args) throws IOException {
        Connection conn = HbaseUtil.getConn();

        BufferedMutator bm = conn.getBufferedMutator(TableName.valueOf("doit34:user_info"));

        Put put1 = new Put(Bytes.toBytes("013"));
        put1.addColumn(Bytes.toBytes("base_info"), Bytes.toBytes("name"), Bytes.toBytes("李冰冰"));
        put1.addColumn(Bytes.toBytes("base_info"), Bytes.toBytes("gender"), Bytes.toBytes("F"));
        put1.addColumn(Bytes.toBytes("base_info"), Bytes.toBytes("age"), Bytes.toBytes("50"));
        put1.addColumn(Bytes.toBytes("ext_info"), Bytes.toBytes("movie"), Bytes.toBytes("少年包青天"));
        put1.addColumn(Bytes.toBytes("ext_info"), Bytes.toBytes("boyfriend"), Bytes.toBytes("NO"));


        Put put2 = new Put(Bytes.toBytes("014"));
        put2.addColumn(Bytes.toBytes("base_info"), Bytes.toBytes("name"), Bytes.toBytes("李爱成"));
        put2.addColumn(Bytes.toBytes("base_info"), Bytes.toBytes("gender"), Bytes.toBytes("M"));
        put2.addColumn(Bytes.toBytes("base_info"), Bytes.toBytes("age"), Bytes.toBytes("20"));
        put2.addColumn(Bytes.toBytes("ext_info"), Bytes.toBytes("goods"), Bytes.toBytes("小牛炖牛肉"));
        put2.addColumn(Bytes.toBytes("ext_info"), Bytes.toBytes("money"), Bytes.toBytes("8"));

        List<Put> list = Arrays.asList(put1, put2);

        bm.mutate(list);



//        bm.flush();

        // 也能刷出数据
        bm.close();
        conn.close();

    }
}
