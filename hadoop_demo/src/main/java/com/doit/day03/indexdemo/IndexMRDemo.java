package com.doit.day03.indexdemo;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.io.IOException;

/*
hello a.txt-2 b.txt-2 c.txt-3 d.txt-2
world a.txt-1 b.txt-1 c.txt-1 d.txt-2
 */
public class IndexMRDemo {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setMapperClass(IndexMapper2.class);
        job.setReducerClass(IndexReducer2.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);


        job.setOutputFormatClass(SequenceFileOutputFormat.class);


        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\index"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\index_value"));

        job.waitForCompletion(true);
    }

    static class IndexMapper2 extends Mapper<LongWritable, Text, Text, IntWritable> {
        String fileName;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            FileSplit fs = (FileSplit) context.getInputSplit();

            fileName = fs.getPath().getName();
        }

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String s = value.toString();

            String[] sp = s.split("\\s+");

            for (String ss : sp) {
                context.write(new Text(ss + "-" + fileName), new IntWritable(1));
            }

        }
    }

    static class IndexReducer2 extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int count = 0;
            for (IntWritable value : values) {
                count++;
            }

            context.write(key, new IntWritable(count));
        }


    }
}
