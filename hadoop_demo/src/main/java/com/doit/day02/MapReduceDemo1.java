package com.doit.day02;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class MapReduceDemo1 {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        //将map和reduce任务设置进来
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);

        //设置Map的输出泛型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        //设置reduce端的输出泛型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\wdcount.txt"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\wdcount_value"));

        job.waitForCompletion(true);


    }
}

/*
keyIn : 每行数据的偏移量  上面所有的数据的字节数 + 1
valueIn: 每一行的内容

keyOut: 每个单词
valueOut: 单词的次数

 */

class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    // key  代表的是偏移量  value 代表的是每一行的数据内容
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        //1.先将value转为String
        String s = value.toString();

        String[] sp = s.split("\\s+");

        for (String ss : sp) {
            Text k = new Text(ss);
            IntWritable v = new IntWritable(1);

            //将数据传递给reduce端
            context.write(k, v);
        }
    }
}

/*
keyIn : 跟Map端的keyOut一致
valueIn: 跟Map端的valueOut一致

keyOut: 聚合后的key
valueOut: 单词的总次数
 */

class WordCountReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
    // key  map端的输出 key    a
    // values    <1,1,1,1,1,1,1,1,1,1,1,1,1,1,1>
    // 分组聚合
    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value : values) {
            int i = value.get();
            sum += i;
        }

        context.write(key, new IntWritable(sum));
    }
}