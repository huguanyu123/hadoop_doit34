package com.doit.day03;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

/*
hello a.txt-2 b.txt-2 c.txt-3 d.txt-2
world a.txt-1 b.txt-1 c.txt-1 d.txt-2
 */
public class IndexMRDemo2 {
    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        Job job = Job.getInstance(conf);

        job.setMapperClass(IndexMapper2.class);
        job.setReducerClass(IndexReducer2.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);


        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job, new Path("D:\\hdp_data\\index_value"));
        FileOutputFormat.setOutputPath(job, new Path("D:\\hdp_data\\index_value2"));

        job.waitForCompletion(true);
    }

    static class IndexMapper2 extends Mapper<LongWritable, Text, Text, Text> {

        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String s = value.toString();

            String[] sp = s.split("-");

            context.write(new Text(sp[0]), new Text(sp[1]));

        }
    }

    static class IndexReducer2 extends Reducer<Text, Text, Text, Text> {
        @Override
        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            StringBuilder sb = new StringBuilder();

            for (Text value : values) {
                String[] sp = value.toString().split("\\s+");

                sb.append(sp[0]).append("--").append(sp[1]).append("\t");
            }
            context.write(key, new Text(sb.toString()));
        }
    }
}
