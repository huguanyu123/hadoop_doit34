package com.doit.day01;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URISyntaxException;

public class HDFSFileDemo4 {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {

        FileSystem fs = FsUtil.getFileSystem();

        Path path = new Path("/huguanyu/hello/world");

        //创建目录
//        fs.mkdirs(path);

        //创建文件
//        fs.create(new Path("/Hello.java"));

        System.out.println(fs.isDirectory(path));
        System.out.println(fs.isFile(path));

        fs.close();

    }
}
